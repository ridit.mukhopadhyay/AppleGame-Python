import pygame
import random
import time
from pygame.locals import (
    K_LEFT,
    K_RIGHT,
    RLEACCEL,
    
    )


pygame.init()

color_white = (255,255,255)
color_black = (0,0,0)
color_skyblue = (135, 206, 235)

fps = 120
clock = pygame.time.Clock()
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

background = pygame.image.load("backgroundapple.png")

game_window = pygame.display.set_mode((SCREEN_WIDTH,SCREEN_HEIGHT))
pygame.display.set_caption("Apple Game")

class Apple(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        #self.image = pygame.image.load("apple.png").convert()
        self.image = pygame.image.load("apple.png").convert_alpha()
        #self.image.set_colorkey((135, 206, 235), RLEACCEL)
        self.rect = self.image.get_rect(center = (random.randint(50,700),50))
        self.change_y = 3
        
    def update(self):
        self.rect.y += self.change_y
        if(self.rect.y > SCREEN_HEIGHT - 50):
            self.rect.centerx = random.randint(50,700)
            self.rect.centery = 3
            score.scorevalue -=1
            if score.scorevalue == -2:
                self.change_y = 0
            
        collision = pygame.sprite.spritecollideany(apple, bowl_sprite)
        if collision:
            self.rect.centerx = random.randint(50,700)
            self.rect.centery = 50
            score.scorevalue +=1
            if score.scorevalue == 10:
                self.change_y = 0
            
                
            
            
            
            
class Bowl(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("bowl.png").convert()
        self.image.set_colorkey(color_black, RLEACCEL)
        self.rect = self.image.get_rect(center = (random.randint(50,700),SCREEN_HEIGHT - 50))
        self.changex = 0
        
        
    def update(self):
        self.changex = 0
        keystate = pygame.key.get_pressed()
        if keystate[K_LEFT]:
            self.changex = -5
            
        if keystate[K_RIGHT]:
            self.changex = 5
            
        self.rect.x += self.changex
        
        
        
class Score():
    def __init__(self):
        self.scorevalue = 0
        self.scorefont = pygame.font.SysFont(None,100)
        self.you_win_font = pygame.font.SysFont(None,50)
        
        
    def update(self):
        self.scoreToBePainted = self.scorefont.render(str(self.scorevalue),True,color_black,color_skyblue)
        self.you_win = self.you_win_font.render("YOU WIN",True,color_black,color_skyblue)
        self.you_lose = self.you_win_font.render("YOU LOSE",True,color_black,color_skyblue)
        
        
    def draw(self):
        game_window.blit(self.scoreToBePainted,(SCREEN_WIDTH/6,SCREEN_HEIGHT/8))
        if self.scorevalue == 10:
            game_window.blit(self.you_win,(SCREEN_WIDTH/2 - 50,SCREEN_HEIGHT/7))
            
        if self.scorevalue == -2:
            game_window.blit(self.you_lose,(SCREEN_WIDTH/2 - 50,SCREEN_HEIGHT/7))
        
        
        
        
        
    
            
apple_sprite = pygame.sprite.GroupSingle()
bowl_sprite = pygame.sprite.GroupSingle()
            
            
            
apple = Apple()
apple_sprite.add(apple)         
bowl = Bowl()
bowl_sprite.add(bowl)
score = Score()
    


running = True
while running:
    clock.tick(fps)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
            
    apple_sprite.update()
    bowl_sprite.update()
    score.update()
    game_window.fill(color_white)
    game_window.blit(background,(0,0))
    score.draw()
    apple_sprite.draw(game_window)
    bowl_sprite.draw(game_window)
    pygame.display.flip()
